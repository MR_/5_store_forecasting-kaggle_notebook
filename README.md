# Store Item Demand Forecasting Challenge 

This is a Notebook for the Kaggle competition, Store Item Demand Forecasting Challenge: 

https://www.kaggle.com/competitions/demand-forecasting-kernels-only

## Goal

The goal of notebook is to predict 3 months of sales for 50 different items at 10 different stores using the last 5 years. 

## Evaluation

The evalutation is based on SMAPE:

$`\frac{100\%}{n}\displaystyle\sum_{t=1}^{n} \frac{|Ft-At|}{(|At|+|Ft|)/2}`$

## Main Contents 

- Generalized additive model

## Kaggle's notebook:

https://www.kaggle.com/code/mr0024/store-forecasting-using-gam

## Requirements

The requirements file contains all the packages needed to work through this notebook
